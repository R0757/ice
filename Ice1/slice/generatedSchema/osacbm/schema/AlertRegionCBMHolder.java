// **********************************************************************
//
// Copyright (c) 2003-2013 ZeroC, Inc. All rights reserved.
//
// This copy of Ice is licensed to you under the terms described in the
// ICE_LICENSE file included in this distribution.
//
// **********************************************************************
//
// Ice version 3.5.1
//
// <auto-generated>
//
// Generated from file `schema.ice'
//
// Warning: do not edit this file.
//
// </auto-generated>
//

package osacbm.schema;

public final class AlertRegionCBMHolder extends Ice.ObjectHolderBase<AlertRegionCBM>
{
    public
    AlertRegionCBMHolder()
    {
    }

    public
    AlertRegionCBMHolder(AlertRegionCBM value)
    {
        this.value = value;
    }

    public void
    patch(Ice.Object v)
    {
        if(v == null || v instanceof AlertRegionCBM)
        {
            value = (AlertRegionCBM)v;
        }
        else
        {
            IceInternal.Ex.throwUOE(type(), v);
        }
    }

    public String
    type()
    {
        return AlertRegionCBM.ice_staticId();
    }
}
