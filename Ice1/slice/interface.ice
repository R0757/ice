#ifndef OSACBM_INTERFACES
#define OSACBM_INTERFACES

#include "schema.ice"

module osacbm {
	module interfaces {
		module DataEventServer {
			
			
			interface Printer {
			
				::osacbm::schema::DataEvent doOSACBM(::osacbm::schema::DataEvent de);
			};
			
	        	
		};
	};
};

#endif