/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package osacbm.module;

import Ice.Current;
import java.util.ArrayList;
import java.util.List;
import osacbm.interfaces.DataEventServer.PrinterPrx;
import osacbm.interfaces.DataEventServer.PrinterPrxHelper;
import osacbm.schema.*;
/**
 *
 * @author s223586
 */
public class Client {
    public static void main(String[] args)
    {    
    int status = 0;
    Ice.Communicator ic = null;
    
	try {
    ic = Ice.Util.initialize(args);
    Ice.ObjectPrx base = ic.stringToProxy("SimplePrinter:default -h localhost -p 10000");
    PrinterPrx printer = PrinterPrxHelper.checkedCast(base);

    System.out.println("OSACBM Test...");
    // Create object of DAWaveform Class
    DAWaveform de = new DAWaveform();
    // Set value of ID in DAWaveform class
    de.setId(new LongValue(22));
		
  ///System.out.println("ID IS : " + de.getId().getValue());
	  
    // Create a list to set values in DAWaveform Class method java.util.List<DoubleValue> setValues()
    List list1 = new ArrayList();
    for(int j=0;j<1;j++)
    {
    for (int i=0; i<4; i++){
    list1.add(new DoubleValue(i));
    }
    }
    
	de.setValues(list1);
 
  ///for (int i=0; i<de.getValues().size(); i++)
  ///{
  ///System.out.print("values :" +de.getValues().get(i) + "   ");
  ///}
  ///System.out.print("\n");
             
        // Calling (DataEvent doOSACBM(DataEvent de) method) and call DAWaveform internal methods            
        DAWaveform rul = (DAWaveform)(printer.doOSACBM((DataEvent)de));   // Error no statement run after this
		System.out.println("I am there");
        System.out.println(rul);
		// retrieve value of getID from DAWaveform class
        System.out.println("rul id = " + rul.getId());
		// retrieve values from DAWaveform class 
        System.out.print("rul values = ");
        for (int i=0; i<rul.getValues().size(); i++)
        {
        System.out.print(rul.getValues().get(i) + "   ");
        }
        System.out.print("\n");
        } 
    

    catch (Ice.LocalException e) {
    e.printStackTrace();
    status = 1;
    } 
    
	catch (Exception e) {
    System.err.println(e.getMessage());
    status = 1;
    }
	
    if (ic != null) {
    // Clean up    
	try {
    ic.destroy();
    } 
    
	catch (Exception e) {
    System.err.println(e.getMessage());
    status = 1;
    }
    }
    System.exit(status);
}
}

