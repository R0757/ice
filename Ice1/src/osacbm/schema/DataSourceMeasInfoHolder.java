// **********************************************************************
//
// Copyright (c) 2003-2013 ZeroC, Inc. All rights reserved.
//
// This copy of Ice is licensed to you under the terms described in the
// ICE_LICENSE file included in this distribution.
//
// **********************************************************************
//
// Ice version 3.5.1
//
// <auto-generated>
//
// Generated from file `schema.ice'
//
// Warning: do not edit this file.
//
// </auto-generated>
//

package osacbm.schema;

public final class DataSourceMeasInfoHolder extends Ice.ObjectHolderBase<DataSourceMeasInfo>
{
    public
    DataSourceMeasInfoHolder()
    {
    }

    public
    DataSourceMeasInfoHolder(DataSourceMeasInfo value)
    {
        this.value = value;
    }

    public void
    patch(Ice.Object v)
    {
        if(v == null || v instanceof DataSourceMeasInfo)
        {
            value = (DataSourceMeasInfo)v;
        }
        else
        {
            IceInternal.Ex.throwUOE(type(), v);
        }
    }

    public String
    type()
    {
        return DataSourceMeasInfo.ice_staticId();
    }
}
