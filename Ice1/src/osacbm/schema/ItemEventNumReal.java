// **********************************************************************
//
// Copyright (c) 2003-2013 ZeroC, Inc. All rights reserved.
//
// This copy of Ice is licensed to you under the terms described in the
// ICE_LICENSE file included in this distribution.
//
// **********************************************************************
//
// Ice version 3.5.1
//
// <auto-generated>
//
// Generated from file `schema.ice'
//
// Warning: do not edit this file.
//
// </auto-generated>
//

package osacbm.schema;

public class ItemEventNumReal extends Ice.ObjectImpl
{
    public ItemEventNumReal()
    {
    }

    public ItemEventNumReal(MIMKey3 evNumDataType, EngUnit engUnitValue, DoubleValue dataValue)
    {
        this.evNumDataType = evNumDataType;
        this.engUnitValue = engUnitValue;
        this.dataValue = dataValue;
    }

    private static class __F implements Ice.ObjectFactory
    {
        public Ice.Object create(String type)
        {
            assert(type.equals(ice_staticId()));
            return new ItemEventNumReal();
        }

        public void destroy()
        {
        }
    }
    private static Ice.ObjectFactory _factory = new __F();

    public static Ice.ObjectFactory
    ice_factory()
    {
        return _factory;
    }

    public static final String[] __ids =
    {
        "::Ice::Object",
        "::osacbm::schema::ItemEventNumReal"
    };

    public boolean ice_isA(String s)
    {
        return java.util.Arrays.binarySearch(__ids, s) >= 0;
    }

    public boolean ice_isA(String s, Ice.Current __current)
    {
        return java.util.Arrays.binarySearch(__ids, s) >= 0;
    }

    public String[] ice_ids()
    {
        return __ids;
    }

    public String[] ice_ids(Ice.Current __current)
    {
        return __ids;
    }

    public String ice_id()
    {
        return __ids[1];
    }

    public String ice_id(Ice.Current __current)
    {
        return __ids[1];
    }

    public static String ice_staticId()
    {
        return __ids[1];
    }

    protected void __writeImpl(IceInternal.BasicStream __os)
    {
        __os.startWriteSlice(ice_staticId(), -1, true);
        __os.writeObject(evNumDataType);
        __os.writeObject(engUnitValue);
        __os.writeObject(dataValue);
        __os.endWriteSlice();
    }

    private class Patcher implements IceInternal.Patcher
    {
        Patcher(int member)
        {
            __member = member;
        }

        public void
        patch(Ice.Object v)
        {
            switch(__member)
            {
            case 0:
                __typeId = "::osacbm::schema::MIMKey3";
                if(v == null || v instanceof MIMKey3)
                {
                    evNumDataType = (MIMKey3)v;
                }
                else
                {
                    IceInternal.Ex.throwUOE(type(), v);
                }
                break;
            case 1:
                __typeId = "::osacbm::schema::EngUnit";
                if(v == null || v instanceof EngUnit)
                {
                    engUnitValue = (EngUnit)v;
                }
                else
                {
                    IceInternal.Ex.throwUOE(type(), v);
                }
                break;
            case 2:
                __typeId = "::osacbm::schema::DoubleValue";
                if(v == null || v instanceof DoubleValue)
                {
                    dataValue = (DoubleValue)v;
                }
                else
                {
                    IceInternal.Ex.throwUOE(type(), v);
                }
                break;
            }
        }

        public String
        type()
        {
            return __typeId;
        }

        private int __member;
        private String __typeId;
    }

    protected void __readImpl(IceInternal.BasicStream __is)
    {
        __is.startReadSlice();
        __is.readObject(new Patcher(0));
        __is.readObject(new Patcher(1));
        __is.readObject(new Patcher(2));
        __is.endReadSlice();
    }

    protected MIMKey3 evNumDataType;

    public MIMKey3
    getEvNumDataType()
    {
        return evNumDataType;
    }

    public void
    setEvNumDataType(MIMKey3 _evNumDataType)
    {
        evNumDataType = _evNumDataType;
    }

    protected EngUnit engUnitValue;

    public EngUnit
    getEngUnitValue()
    {
        return engUnitValue;
    }

    public void
    setEngUnitValue(EngUnit _engUnitValue)
    {
        engUnitValue = _engUnitValue;
    }

    protected DoubleValue dataValue;

    public DoubleValue
    getDataValue()
    {
        return dataValue;
    }

    public void
    setDataValue(DoubleValue _dataValue)
    {
        dataValue = _dataValue;
    }

    public static final long serialVersionUID = -1118946604L;
}
