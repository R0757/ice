// **********************************************************************
//
// Copyright (c) 2003-2013 ZeroC, Inc. All rights reserved.
//
// This copy of Ice is licensed to you under the terms described in the
// ICE_LICENSE file included in this distribution.
//
// **********************************************************************
//
// Ice version 3.5.1
//
// <auto-generated>
//
// Generated from file `schema.ice'
//
// Warning: do not edit this file.
//
// </auto-generated>
//

package osacbm.schema;

public class OsacbmTime extends Ice.ObjectImpl
{
    public OsacbmTime()
    {
    }

    public OsacbmTime(LocalTime localTimeValue, OsacbmTimeType timeType, String time, BigIntValue timeBinary)
    {
        this.localTimeValue = localTimeValue;
        this.timeType = timeType;
        this.time = time;
        this.timeBinary = timeBinary;
    }

    private static class __F implements Ice.ObjectFactory
    {
        public Ice.Object create(String type)
        {
            assert(type.equals(ice_staticId()));
            return new OsacbmTime();
        }

        public void destroy()
        {
        }
    }
    private static Ice.ObjectFactory _factory = new __F();

    public static Ice.ObjectFactory
    ice_factory()
    {
        return _factory;
    }

    public static final String[] __ids =
    {
        "::Ice::Object",
        "::osacbm::schema::OsacbmTime"
    };

    public boolean ice_isA(String s)
    {
        return java.util.Arrays.binarySearch(__ids, s) >= 0;
    }

    public boolean ice_isA(String s, Ice.Current __current)
    {
        return java.util.Arrays.binarySearch(__ids, s) >= 0;
    }

    public String[] ice_ids()
    {
        return __ids;
    }

    public String[] ice_ids(Ice.Current __current)
    {
        return __ids;
    }

    public String ice_id()
    {
        return __ids[1];
    }

    public String ice_id(Ice.Current __current)
    {
        return __ids[1];
    }

    public static String ice_staticId()
    {
        return __ids[1];
    }

    protected void __writeImpl(IceInternal.BasicStream __os)
    {
        __os.startWriteSlice(ice_staticId(), -1, true);
        __os.writeObject(localTimeValue);
        timeType.__write(__os);
        __os.writeString(time);
        __os.writeObject(timeBinary);
        __os.endWriteSlice();
    }

    private class Patcher implements IceInternal.Patcher
    {
        Patcher(int member)
        {
            __member = member;
        }

        public void
        patch(Ice.Object v)
        {
            switch(__member)
            {
            case 0:
                __typeId = "::osacbm::schema::LocalTime";
                if(v == null || v instanceof LocalTime)
                {
                    localTimeValue = (LocalTime)v;
                }
                else
                {
                    IceInternal.Ex.throwUOE(type(), v);
                }
                break;
            case 1:
                __typeId = "::osacbm::schema::BigIntValue";
                if(v == null || v instanceof BigIntValue)
                {
                    timeBinary = (BigIntValue)v;
                }
                else
                {
                    IceInternal.Ex.throwUOE(type(), v);
                }
                break;
            }
        }

        public String
        type()
        {
            return __typeId;
        }

        private int __member;
        private String __typeId;
    }

    protected void __readImpl(IceInternal.BasicStream __is)
    {
        __is.startReadSlice();
        __is.readObject(new Patcher(0));
        timeType = OsacbmTimeType.__read(__is);
        time = __is.readString();
        __is.readObject(new Patcher(1));
        __is.endReadSlice();
    }

    protected LocalTime localTimeValue;

    public LocalTime
    getLocalTimeValue()
    {
        return localTimeValue;
    }

    public void
    setLocalTimeValue(LocalTime _localTimeValue)
    {
        localTimeValue = _localTimeValue;
    }

    protected OsacbmTimeType timeType;

    public OsacbmTimeType
    getTimeType()
    {
        return timeType;
    }

    public void
    setTimeType(OsacbmTimeType _timeType)
    {
        timeType = _timeType;
    }

    protected String time;

    public String
    getTime()
    {
        return time;
    }

    public void
    setTime(String _time)
    {
        time = _time;
    }

    protected BigIntValue timeBinary;

    public BigIntValue
    getTimeBinary()
    {
        return timeBinary;
    }

    public void
    setTimeBinary(BigIntValue _timeBinary)
    {
        timeBinary = _timeBinary;
    }

    public static final long serialVersionUID = -1213115868L;
}
