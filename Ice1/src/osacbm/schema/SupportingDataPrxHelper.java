// **********************************************************************
//
// Copyright (c) 2003-2013 ZeroC, Inc. All rights reserved.
//
// This copy of Ice is licensed to you under the terms described in the
// ICE_LICENSE file included in this distribution.
//
// **********************************************************************
//
// Ice version 3.5.1
//
// <auto-generated>
//
// Generated from file `schema.ice'
//
// Warning: do not edit this file.
//
// </auto-generated>
//

package osacbm.schema;

public final class SupportingDataPrxHelper extends Ice.ObjectPrxHelperBase implements SupportingDataPrx
{
    public static SupportingDataPrx checkedCast(Ice.ObjectPrx __obj)
    {
        SupportingDataPrx __d = null;
        if(__obj != null)
        {
            if(__obj instanceof SupportingDataPrx)
            {
                __d = (SupportingDataPrx)__obj;
            }
            else
            {
                if(__obj.ice_isA(ice_staticId()))
                {
                    SupportingDataPrxHelper __h = new SupportingDataPrxHelper();
                    __h.__copyFrom(__obj);
                    __d = __h;
                }
            }
        }
        return __d;
    }

    public static SupportingDataPrx checkedCast(Ice.ObjectPrx __obj, java.util.Map<String, String> __ctx)
    {
        SupportingDataPrx __d = null;
        if(__obj != null)
        {
            if(__obj instanceof SupportingDataPrx)
            {
                __d = (SupportingDataPrx)__obj;
            }
            else
            {
                if(__obj.ice_isA(ice_staticId(), __ctx))
                {
                    SupportingDataPrxHelper __h = new SupportingDataPrxHelper();
                    __h.__copyFrom(__obj);
                    __d = __h;
                }
            }
        }
        return __d;
    }

    public static SupportingDataPrx checkedCast(Ice.ObjectPrx __obj, String __facet)
    {
        SupportingDataPrx __d = null;
        if(__obj != null)
        {
            Ice.ObjectPrx __bb = __obj.ice_facet(__facet);
            try
            {
                if(__bb.ice_isA(ice_staticId()))
                {
                    SupportingDataPrxHelper __h = new SupportingDataPrxHelper();
                    __h.__copyFrom(__bb);
                    __d = __h;
                }
            }
            catch(Ice.FacetNotExistException ex)
            {
            }
        }
        return __d;
    }

    public static SupportingDataPrx checkedCast(Ice.ObjectPrx __obj, String __facet, java.util.Map<String, String> __ctx)
    {
        SupportingDataPrx __d = null;
        if(__obj != null)
        {
            Ice.ObjectPrx __bb = __obj.ice_facet(__facet);
            try
            {
                if(__bb.ice_isA(ice_staticId(), __ctx))
                {
                    SupportingDataPrxHelper __h = new SupportingDataPrxHelper();
                    __h.__copyFrom(__bb);
                    __d = __h;
                }
            }
            catch(Ice.FacetNotExistException ex)
            {
            }
        }
        return __d;
    }

    public static SupportingDataPrx uncheckedCast(Ice.ObjectPrx __obj)
    {
        SupportingDataPrx __d = null;
        if(__obj != null)
        {
            if(__obj instanceof SupportingDataPrx)
            {
                __d = (SupportingDataPrx)__obj;
            }
            else
            {
                SupportingDataPrxHelper __h = new SupportingDataPrxHelper();
                __h.__copyFrom(__obj);
                __d = __h;
            }
        }
        return __d;
    }

    public static SupportingDataPrx uncheckedCast(Ice.ObjectPrx __obj, String __facet)
    {
        SupportingDataPrx __d = null;
        if(__obj != null)
        {
            Ice.ObjectPrx __bb = __obj.ice_facet(__facet);
            SupportingDataPrxHelper __h = new SupportingDataPrxHelper();
            __h.__copyFrom(__bb);
            __d = __h;
        }
        return __d;
    }

    public static final String[] __ids =
    {
        "::Ice::Object",
        "::osacbm::schema::SupportingData"
    };

    public static String ice_staticId()
    {
        return __ids[1];
    }

    protected Ice._ObjectDelM __createDelegateM()
    {
        return new _SupportingDataDelM();
    }

    protected Ice._ObjectDelD __createDelegateD()
    {
        return new _SupportingDataDelD();
    }

    public static void __write(IceInternal.BasicStream __os, SupportingDataPrx v)
    {
        __os.writeProxy(v);
    }

    public static SupportingDataPrx __read(IceInternal.BasicStream __is)
    {
        Ice.ObjectPrx proxy = __is.readProxy();
        if(proxy != null)
        {
            SupportingDataPrxHelper result = new SupportingDataPrxHelper();
            result.__copyFrom(proxy);
            return result;
        }
        return null;
    }

    public static final long serialVersionUID = 0L;
}
